# Copyright (C) 2018-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import date, timedelta

from django.core.mail import send_mail
from django.core.management import BaseCommand
from django.db.models import Q
from django.template.loader import render_to_string
from django.utils.translation import activate
from note.models import Note


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--spam", "-s", action='store_true', help="Spam negative users")
        parser.add_argument("--report", "-r", action='store_true', help="Report the list of negative users to admins")
        parser.add_argument("--negative-amount", "-n", action='store', type=int, default=1000,
                            help="Maximum amount to be considered as very negative (inclusive)")
        parser.add_argument("--add-years", "-y", action='store', type=int, default=0,
                            help="Add also people that have a negative balance since N years "
                                 "(default to only active members)")

    def handle(self, *args, **options):
        activate('fr')

        if options['negative_amount'] == 0:
            # Don't log empty notes
            options['negative_amount'] = 1

        notes = Note.objects.filter(
            Q(noteuser__user__memberships__date_end__gte=
              date.today() - timedelta(days=int(365.25 * options['add_years'])))
            | (Q(noteclub__isnull=False) & ~Q(noteclub__club__name__icontains='- BDE')),
            balance__lte=-options["negative_amount"],
            is_active=True, 
        ).order_by('balance').distinct().all()

        if options["spam"]:
            for note in notes:
                note.send_mail_negative_balance()

        if options["report"]:
            plain_text = render_to_string("note/mails/negative_notes_report.txt", context=dict(notes=notes))
            html = render_to_string("note/mails/negative_notes_report.html", context=dict(notes=notes))
            send_mail("[Note Kfet] Liste des négatifs", plain_text, "Note Kfet 2020 <notekfet2020@crans.org>",
                      recipient_list=["respo-info.bde@lists.crans.org", "tresorerie.bde@lists.crans.org"],
                      html_message=html)
