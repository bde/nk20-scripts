# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import date

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from member.models import Club, Membership
from django.core.mail import send_mail


class Command(BaseCommand):
    help = "Get mailing list registrations from the last wei. " \
           "Usage: manage.py extract_ml_registrations -t {events,art,sport} -l {fr, en} -y {0, 1, ...}. " \
           "You can write this into a file with a pipe, then paste the document into your mail manager."

    def add_arguments(self, parser):
        parser.add_argument('--type', '-t', choices=["members", "clubs", "events", "art", "sport"], default="members",
                            help='Select the type of the mailing list (default members)')
        parser.add_argument('--lang', '-l', type=str, choices=['fr', 'en'], default='fr',
                            help='Select the registred users of the ML of the given language. Useful only for the '
                                 'events mailing list.')
        parser.add_argument('--years', '-y', type=int, default=0, 
                            help='Select the cumulative registred users of a membership from years ago. 0 means the current users')
        parser.add_argument('--email', '-e', type=str, default="", 
                            help='Put the email supposed to receive the emails of the mailing list (only for art). If nothing    is put, the script will just print the emails.')

    def handle(self, *args, **options):
        # TODO: Improve the mailing list extraction system, and link it automatically with Mailman.

        if options['verbosity'] == 0:
            # This is useless, but this what the user asked.
            return

        if options["type"] == "members":
            today_date = date.today()
            selected_date = date(today_date.year - options["years"], today_date.month, today_date.day)
            for membership in Membership.objects.filter(
                club__name="BDE",
                date_start__lte=today_date,
                date_end__gte=selected_date,
            ).all():
                self.stdout.write(membership.user.email)
            return

        if options["type"] == "clubs":
            for club in Club.objects.all():
                self.stdout.write(club.email)
            return

        # Get the list of mails that want to be registered to the events mailing listn, as well as the number of mails.
        # Print it or send it to the email provided by the user.
        # Don't filter to valid members, old members can receive these mails as long as they want.
        if options["type"] == "events":
            nb=0

            if options["email"] == "":
                for user in User.objects.filter(profile__ml_events_registration=options["lang"]).all():
                    self.stdout.write(user.email)     
                    nb+=1
                self.stdout.write(str(nb))

            else :
                emails = []
                for user in User.objects.filter(profile__ml_events_registration=options["lang"]).all():
                    emails.append(user.email)
                    nb+=1

                subject = "Liste des abonnés à la newsletter BDE"
                message = (
                    f"Voici la liste des utilisateurs abonnés à la newsletter BDE:\n\n"
                    + "\n".join(emails) 
                    + f"\n\nTotal des abonnés : {nb}"
                )
                from_email = "Note Kfet 2020 <notekfet2020@crans.org>" 
                recipient_list = [options["email"]] 

                send_mail(subject, message, from_email, recipient_list)

            return

        if options["type"] == "art":
            nb=0

            if options["email"] == "":
                for user in User.objects.filter(profile__ml_art_registration=True).all():
                    self.stdout.write(user.email)     
                    nb+=1
                self.stdout.write(str(nb))

            else :
                emails = []
                for user in User.objects.filter(profile__ml_art_registration=True).all():
                    emails.append(user.email)
                    nb+=1

                subject = "Liste des abonnés à la newsletter BDA"
                message = (
                    f"Voici la liste des utilisateurs abonnés à la newsletter BDA:\n\n"
                    + "\n".join(emails) 
                    + f"\n\nTotal des abonnés : {nb}"
                )
                from_email = "Note Kfet 2020 <notekfet2020@crans.org>" 
                recipient_list = [options["email"]] 

                send_mail(subject, message, from_email, recipient_list)

            return

        if options["type"] == "sport":
            nb=0

            if options["email"] == "":
                for user in User.objects.filter(profile__ml_sport_registration=True).all():
                    self.stdout.write(user.email)     
                    nb+=1
                self.stdout.write(str(nb))

            else :
                emails = []
                for user in User.objects.filter(profile__ml_sport_registration=True).all():
                    emails.append(user.email)
                    nb+=1

                subject = "Liste des abonnés à la newsletter BDS"
                message = (
                    f"Voici la liste des utilisateurs abonnés à la newsletter BDS:\n\n"
                    + "\n".join(emails) 
                    + f"\n\nTotal des abonnés : {nb}"
                )
                from_email = "Note Kfet 2020 <notekfet2020@crans.org>" 
                recipient_list = [options["email"]] 

                send_mail(subject, message, from_email, recipient_list)

            return
