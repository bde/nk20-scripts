# Copyright (C) 2018-2023 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    """
    Command to protect sensitive data during the beta phase or after WEI.
    Phone number, email address, postal address, first and last name,
    IP addresses, health issues, gender and birth date are removed.
    """
    def add_arguments(self, parser):
        parser.add_argument('--force', '-f', action='store_true', help="Actually anonymize data.")
        parser.add_argument('--type', '-t', choices=["all", "wei", "user"], default="",
                            help='Select the type of data to anonyze (default None)')

    def handle(self, *args, **options):
        if not options['force']:
            if options['type'] == "all":
                self.stderr.write("CAUTION: This is a dangerous script. This will reset ALL personal data with "
                              "sample data. Don't use  in production! If you know what you are doing, please "
                              "add --force option.")
            elif options['type'] == "wei":
                self.stderr.write("CAUTION: This is a dangerous script. This will reset WEI personal data with "
                              "sample data. Use it in production only after WEI. If you know what you are doing,"
                              "please add --force option.")
            elif options['type'] == "user":
                self.stderr.write("CAUTION: This is a dangerous script. This will reset all personal data "
                              "visible by user (not admin or trez BDE) with sample data. Don't use  in "
                              "production! If you know what you are doing, please add --force option.")
            else:
                self.stderr.write("CAUTION: This is a dangerous script. This will reset all personal data with "
                              "sample data. Don't use  in production ('wei' can be use in production after "
                              "the WEI)! If you know what you are doing, please choose a type.")
            exit(1)

        cur = connection.cursor()
        if options['type'] in ("all","user"):
            if options['verbosity'] != 0:
                self.stdout.write("Anonymize profile, user club and guest data")
            cur.execute("UPDATE member_profile SET "
                    "phone_number = '0123456789', "
                    "address = '4 avenue des Sciences, 91190 GIF-SUR-YVETTE';")
            cur.execute("UPDATE auth_user SET "
                    "first_name = 'Anne', "
                    "last_name = 'Onyme', "
                    "email = 'anonymous@example.com';")
            cur.execute("UPDATE member_club SET "
                    "email = 'anonymous@example.com';")
            cur.execute("UPDATE activity_guest SET "
                    "first_name = 'Anne', "
                    "last_name = 'Onyme';")

        if options['type'] in ("all","wei","user"):
            if options['verbosity'] != 0:
                self.stdout.write("Anonymize WEI data")
            cur.execute("UPDATE wei_weiregistration SET "
                    "birth_date = '1998-01-08', "
                    "emergency_contact_name = 'Anne Onyme', "
                    "emergency_contact_phone = '0123456789', "
                    "gender = 'nonbinary', "
                    "health_issues = 'Tout va bien';")

        if options['type'] == "all":
            if options['verbosity'] != 0:
                self.stdout.write("Anonymize invoice, special transaction, log, mailer and oauth data")
            cur.execute("UPDATE treasury_invoice SET "
                    "name = 'Anne Onyme', "
                    "object = 'Rends nous riches', "
                    "description = 'Donne nous plein de sous', "
                    "address = '4 avenue des Sciences, 91190 GIF-SUR-YVETTE';")
            cur.execute("UPDATE treasury_product SET "
                    "designation = 'un truc inutile';")
            cur.execute("UPDATE note_specialtransaction SET "
                    "bank = 'le matelas', "
                    "first_name = 'Anne', "
                    "last_name = 'Onyme';")
            cur.execute("UPDATE logs_changelog SET "
                    "ip = '127.0.0.1', "
                    "data = 'new data', "
                    "previous = 'old data';")
            cur.execute("UPDATE mailer_messagelog SET "
                    "log_message = 'log message', "
                    "message_data = 'message data';")
            cur.execute("UPDATE mailer_dontsendentry SET "
                    "to_address = 'anonymous@example.com';")
            cur.execute("UPDATE oauth2_provider_application SET "
                    "name = 'external app', "
                    "redirect_uris = 'http://external.app', "
                    "client_secret = 'abcdefghijklmnopqrstuvwxyz';")
        cur.close()
