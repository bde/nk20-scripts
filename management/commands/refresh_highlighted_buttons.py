# Copyright (C) 2018-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db.models import Count
from django.utils import timezone
from note.models import RecurrentTransaction, TransactionTemplate


class Command(BaseCommand):
    """
    Command to add the ten most used buttons of the past month to the highlighted buttons.
    """
    def handle(self, *args, **kwargs):
        queryset = RecurrentTransaction.objects.filter(
            template__display=True,
            valid=True,
            created_at__gte=timezone.now() - timedelta(days=30),
        ).values("template").annotate(transaction_count=Count("template")).order_by("-transaction_count")[:10]
        for d in queryset.all():
            button_id = d["template"]
            button = TransactionTemplate.objects.get(pk=button_id)
            if kwargs['verbosity'] > 0:
                self.stdout.write(self.style.WARNING("Highlight button {name} ({count:d} transactions)..."
                                                     .format(name=button.name, count=d["transaction_count"])))
            button.highlighted = True
            button.save()
