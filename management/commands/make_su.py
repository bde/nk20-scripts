# Copyright (C) 2018-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('username', nargs='+', type=str)
        parser.add_argument('-S', "--SUPER", action='store_true', help='make superuser')
        parser.add_argument('-s', "--STAFF", action='store_true', help='make staff')

    def handle(self, *args, **kwargs):
        for uname in kwargs["username"]:
            user = User.objects.get(username=uname)
            user.is_active = True
            if kwargs['STAFF']:
                if kwargs['verbosity'] > 0:
                    self.stdout.write(f"Add {user} to staff users…")
                user.is_staff = True
            if kwargs['SUPER']:
                if kwargs['verbosity'] > 0:
                    self.stdout.write(f"Add {user} to superusers…")
                user.is_superuser = True
            user.save()
