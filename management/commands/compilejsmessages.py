# Copyright (C) 2018-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import translation
from django.views.i18n import JavaScriptCatalog


class Command(BaseCommand):
    """
    Generate Javascript translation files
    """

    def handle(self, *args, **kwargs):
        for code, _ in settings.LANGUAGES:
            if code == settings.LANGUAGE_CODE:
                continue
            if kwargs["verbosity"] > 0:
                self.stdout.write(f"Generate {code} javascript localization file")
            with translation.override(code):
                resp = JavaScriptCatalog().get(None, packages="member+note")
                if not os.path.isdir(settings.STATIC_ROOT + "/js/jsi18n"):
                    os.makedirs(settings.STATIC_ROOT + "/js/jsi18n")
                with open(settings.STATIC_ROOT + f"/js/jsi18n/{code}.js", "wb") as f:
                    f.write(resp.content)
