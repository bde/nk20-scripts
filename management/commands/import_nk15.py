# Copyright (C) 2018-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

import subprocess

from django.core.management import call_command

from ._import_utils import ImportCommand


class Command(ImportCommand):
    """
    Command for importing the database of NK15.
    Need to be run by a user with a registered role in postgres for the database nk15.
    """

    def handle(self, *args, **kwargs):
        subprocess.call("./apps/scripts/shell/tabularasa")

        kwargs["alias"] = True
        kwargs["chunk"] = 1000
        kwargs["save"] = "map.json"
        call_command('import_account', **kwargs)

        del kwargs["alias"]
        del kwargs["save"]
        kwargs["chunk"] = 100
        kwargs["map"] = "map.json"
        call_command('import_activities', **kwargs)

        kwargs["chunk"] = 10000
        kwargs["map"] = "map.json"
        kwargs["buttons"] = True
        call_command('import_transaction', **kwargs)

        call_command('make_su','-sS', 'Coq', 'erdnaxe', 'Krokmou', 'PAC', 'Pollion', 'TLinux', 'ÿnérant')
        call_command('syncsql')
